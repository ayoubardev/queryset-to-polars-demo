import re

import polars as pl
from django.db import models

from demo.utils import database_uri


class ECommerceQuerySet(models.query.QuerySet):
    def to_polars(self):
        queryset = self
        connection_url = database_uri()
        query = queryset.query
        sql, params = query.sql_with_params()
        formatted_sql = sql
        if params:
            formatted_params = tuple((f"'{param}'" for param in params))
            formatted_sql = formatted_sql % formatted_params

        return pl.read_database(query=query, connection=connection_url)


class ECommerceUsersManager(models.Manager):
    def get_queryset(self):
        return ECommerceQuerySet(self.model, using=self._db)


class ECommerceUsers(models.Model):
    event_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    event_type = models.CharField(max_length=50)
    product_id = models.IntegerField()
    category_id = models.BigIntegerField()
    category_code = models.CharField(max_length=255, null=True)
    brand = models.CharField(max_length=50, null=True)
    price = models.DecimalField(max_digits=8, decimal_places=2)
    user_id = models.BigIntegerField()
    user_session = models.CharField(max_length=50, null=True)

    objects = ECommerceUsersManager()
