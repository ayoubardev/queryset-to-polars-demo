import time

from django.core.management.base import BaseCommand

from demo.models import ECommerceUsers


class Command(BaseCommand):
    help = "My shiny new management command."

    def handle(self, *args, **options):
        start_time = time.time()
        df = ECommerceUsers.objects.all().to_polars()
        loading_time = time.time() - start_time
        print(
            f"data loading time is : {loading_time} seconds ~  {loading_time // 60}   minutes"
        )

        print(f"The length of Dataframe  is {df.height}")
